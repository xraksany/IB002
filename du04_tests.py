def test() -> None:
    assert ternary_sum(3, 5) == [17, 23, 25]
    assert ternary_sum(4, 2) == [28, 30, 36, 54]

    # me tests
    assert len(ternary_sum(1, 1)) == 1
    assert len(ternary_sum(1, 2)) == 1
    assert len(ternary_sum(1, 3)) == 0

    assert len(ternary_sum(2, 1)) == 1
    assert len(ternary_sum(2, 2)) == 2
    assert len(ternary_sum(2, 3)) == 2
    assert len(ternary_sum(2, 4)) == 1
    assert len(ternary_sum(2, 5)) == 0

    assert len(ternary_sum(3, 1)) == 1
    assert len(ternary_sum(3, 2)) == 3
    assert len(ternary_sum(3, 3)) == 5
    assert len(ternary_sum(3, 4)) == 5
    assert len(ternary_sum(3, 5)) == 3
    assert len(ternary_sum(3, 6)) == 1
    assert len(ternary_sum(3, 7)) == 0

    assert len(ternary_sum(5, 1)) == 1
    assert len(ternary_sum(5, 2)) == 5
    assert len(ternary_sum(5, 3)) == 14
    assert len(ternary_sum(5, 4)) == 26
    assert len(ternary_sum(5, 5)) == 35
    assert len(ternary_sum(5, 6)) == 35
    assert len(ternary_sum(5, 7)) == 26
    assert len(ternary_sum(5, 8)) == 14
    assert len(ternary_sum(5, 9)) == 5
    assert len(ternary_sum(5, 10)) == 1
    assert len(ternary_sum(5, 11)) == 0

    assert ternary_sum(3, 1) == [9]
    assert ternary_sum(3, 2) == [10, 12, 18]
    assert ternary_sum(3, 3) == [11, 13, 15, 19, 21]
    assert ternary_sum(3, 4) == [14, 16, 20, 22, 24]

    assert ternary_sum(5, 1) == [81]
    assert ternary_sum(5, 2) == [82, 84, 90, 108, 162]

    assert ternary_sum(5, 3) == [83, 85, 87, 91, 93, 99, 109, 111, 117, 135,
                                 163, 165, 171, 189]

    assert ternary_sum(5, 4) == [86, 88, 92, 94, 96, 100, 102, 110, 112, 114,
                                 118, 120, 126, 136, 138, 144, 164, 166, 168,
                                 172, 174, 180, 190, 192, 198, 216]

    assert ternary_sum(5, 5) == [89, 95, 97, 101, 103, 105, 113, 115, 119, 121,
                                 123, 127, 129, 137, 139, 141, 145, 147, 153,
                                 167, 169, 173, 175, 177, 181, 183, 191, 193,
                                 195, 199, 201, 207, 217, 219, 225]

    assert ternary_sum(5, 6) == [98, 104, 106, 116, 122, 124, 128, 130, 132,
                                 140, 142, 146, 148, 150, 154, 156, 170, 176,
                                 178, 182, 184, 186, 194, 196, 200, 202, 204,
                                 208, 210, 218, 220, 222, 226, 228, 234]

    assert ternary_sum(5, 7) == [107, 125, 131, 133, 143, 149, 151, 155, 157,
                                 159, 179, 185, 187, 197, 203, 205, 209, 211,
                                 213, 221, 223, 227, 229, 231, 235, 237]

    assert ternary_sum(5, 8) == [134, 152, 158, 160, 188, 206, 212, 214, 224,
                                 230, 232, 236, 238, 240]

    assert ternary_sum(5, 9) == [161, 215, 233, 239, 241]
    assert ternary_sum(5, 10) == [242]

    return None
