def test() -> None:
    assert is_correct(DMinHeap([], 2))
    assert is_correct(DMinHeap([1, 2, 3, 2, 1, 2, 3, 2], 4))
    assert is_correct(DMinHeap([1, 2, 3, 4, 2, 3, 4, 4, 4, 5, 4, 5, 3], 2))

    assert not is_correct(DMinHeap([42, 17], 4))
    assert not is_correct(DMinHeap([1, 2, 3, 2, 1, 2, 3, 2], 2))
    assert not is_correct(DMinHeap([1, 2, 3, 4, 2, 3, 4, 4, 4, 5, 4, 5, 3], 3))

    heap = DMinHeap([1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1], 2)
    heapify(heap, 1)
    assert heap.array == [1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1]

    heap = DMinHeap([1, 1, 0, 1, 1, 0], 2)
    heapify(heap, 0)
    assert heap.array == [0, 1, 0, 1, 1, 1]

    heap = DMinHeap([5, 1, 2, 3, 4], 2)
    heapify(heap, 0)
    assert heap.array == [1, 3, 2, 5, 4]

    heap = DMinHeap([7, 4, 1, 5, 6, 8, 9, 3, 2, 0, 2, 2], 3)
    heapify(heap, 2)
    assert heap.array == [7, 4, 0, 5, 6, 8, 9, 3, 2, 1, 2, 2]

    heap = DMinHeap([0, 1, 0, 0, 1, 1, 1, 1, 0], 3)
    change_arity(heap, 4)
    assert is_correct(heap)

    heap = DMinHeap([5, 1, 2, 3, 4], 2)
    change_arity(heap, 3)
    assert is_correct(heap)

    heap = DMinHeap([7, 4, 1, 5, 6, 8, 9, 3, 2, 0, 2, 2], 3)
    change_arity(heap, 2)
    assert is_correct(heap)

    heap = DMinHeap([7, 4, 1, 5, 6, 8, 9, 3, 2, 0, 2, 2], 3)
    change_arity(heap, 5)
    assert is_correct(heap)

    heap = DMinHeap([1, 2, 4, 3, 4, 5, 6, 4, 7], 2)
    assert min_three(heap) == (1, 2, 3)

    heap = DMinHeap([1, 3, 3, 2, 4, 4, 4, 5, 5, 5, 6, 6, 2], 3)
    assert min_three(heap) == (1, 2, 2)

    heap = DMinHeap([1, 1, 1], 42)
    assert min_three(heap) == (1, 1, 1)

    assert is_correct(DMinHeap([], 42))
    assert is_correct(DMinHeap([5], 42))
    assert is_correct(DMinHeap([1, 2, 3, 2, 1, 2, 3, 2], 4))
    assert not is_correct(DMinHeap([1, 2, 3, 2, 1, 2, 3, 2], 2))
    assert not is_correct(DMinHeap([1, 2, 3, 4, 2, 3, 4, 4, 4, 5, 4, 5, 3], 3))
    assert is_correct(DMinHeap([1, 2, 3, 4, 2, 3, 4, 4, 4, 5, 4, 5, 3], 2))

    # heapify
    h1 = DMinHeap([5, 1, 2, 3, 4], 2)
    heapify(h1, 0)
    assert h1.array == [1, 3, 2, 5, 4]

    h2 = DMinHeap([7, 4, 1, 5, 6, 8, 9, 3, 2, 0, 2, 2], 3)
    heapify(h2, 2)
    assert h2.array == [7, 4, 0, 5, 6, 8, 9, 3, 2, 1, 2, 2]

    # change arity
    h1 = DMinHeap([1, 2, 3, 4, 3, 3, 5], 3)
    change_arity(h1, 2)
    assert is_correct(h1)
    assert h1.arity == 2

    h2 = DMinHeap([1, 2, 3, 4, 5, 6, 7], 2)
    change_arity(h2, 3)
    assert is_correct(h2)
    assert h2.arity == 3

    h3 = DMinHeap([5, 6, 7, 7, 8, 9, 10, 8, 11, 8, 12, 42], 4)
    change_arity(h3, 2)
    assert is_correct(h3)
    assert h3.arity == 2

    h4 = DMinHeap([3, 10, 4, 5, 12, 13, 15, 5, 10], 3)
    change_arity(h4, 2)
    assert is_correct(h4)
    assert h4.arity == 2

    # min three
    assert min_three(DMinHeap([1, 2, 4, 3, 4, 5, 6, 4, 7], 2)) == (1, 2, 3)
    assert min_three(DMinHeap([1, 4, 3, 2, 4, 4, 4, 5, 5, 5, 6, 6, 2], 3)) \
        == (1, 2, 2)
    assert min_three(DMinHeap([1, 2, 3, 4, 2], 2)) == (1, 2, 2)
    assert min_three(DMinHeap([1, 1, 1], 500)) == (1, 1, 1)
