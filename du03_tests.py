def test() -> None:
    # partition
    assert partition([1, 3, 3, 7], 3) == (1, 3)
    assert partition([1, 3, 3, 7, 17, 42, 69, 420], 11) == (4, 4)

    # me tests
    assert partition([],  0) == (0, 0)

    assert partition([0], 0) == (0, 1)
    assert partition([1], 2) == (1, 1)
    assert partition([1], 0) == (0, 0)

    assert partition([0, 0], 0) == (0, 2)
    assert partition([0, 0], 1) == (2, 2)
    assert partition([1, 1], 0) == (0, 0)

    assert partition([0, 2], 1) == (1, 1)
    assert partition([0, 1], 0) == (0, 1)
    assert partition([0, 1], 1) == (1, 2)

    assert partition([0, 1, 1, 2], 0) == (0, 1)
    assert partition([0, 1, 1, 2], 1) == (1, 3)
    assert partition([0, 1, 1, 2], 2) == (3, 4)

    assert partition([0, 2, 2, 4], 1) == (1, 1)
    assert partition([0, 2, 2, 4], 1) == (1, 1)

    # minimum
    assert minimum([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) == 0
    assert minimum([10, 8, 6, 4, 2, 1, 3, 5, 7, 9]) == 1

    # me tests
    assert minimum([1]) == 1

    assert minimum([0, 1]) == 0
    assert minimum([1, 0]) == 0

    assert minimum([3, 2, 1]) == 1
    assert minimum([1, 2, 3]) == 1
    assert minimum([3, 1, 2]) == 1
    assert minimum([2, 1, 3]) == 1

    assert minimum([5, 4, 3, 1, 2]) == 1
    assert minimum([2, 1, 3, 4, 5]) == 1
