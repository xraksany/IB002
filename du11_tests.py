def draw_example() -> None:
    a = Vertex("A")
    b = Vertex("B")
    c = Vertex("C")
    d = Vertex("D")
    e = Vertex("E")
    f = Vertex("F")
    g = Vertex("G")

    a.succs = [d]
    b.succs = [e, a]
    c.succs = [e, f, g]
    d.succs = [b]
    e.succs = [g, f]

    draw_graph([a, b, c, d, e, f, g], "ib002_graph.dot")


def clean(nodes: List[Vertex]) -> None:
    for node in nodes:
        node.flag = None

    return None


def get_node(a: str) -> int:
    return ord(a) - ord('a')


def test() -> None:
    a = Vertex("A")
    b = Vertex("B")
    c = Vertex("C")
    d = Vertex("D")
    e = Vertex("E")
    f = Vertex("F")
    g = Vertex("G")

    a.succs = [d]
    b.succs = [e, a]
    c.succs = [e, f, g]
    d.succs = [b]
    e.succs = [g, f]

    test_nodes: List[Vertex] = [a, b, c, d, e, f, g]

    assert reachable_size(a) == (6, 6)
    clean(test_nodes)

    assert reachable_size(b) == (6, 6)
    clean(test_nodes)

    assert reachable_size(c) == (4, 5)
    clean(test_nodes)

    assert reachable_size(d) == (6, 6)
    clean(test_nodes)

    assert reachable_size(e) == (3, 2)
    clean(test_nodes)

    assert reachable_size(f) == (1, 0)
    clean(test_nodes)

    assert reachable_size(g) == (1, 0)
    clean(test_nodes)

    assert has_cycle(a)
    clean(test_nodes)

    assert has_cycle(b)
    clean(test_nodes)

    assert not has_cycle(c)
    clean(test_nodes)

    assert has_cycle(d)
    clean(test_nodes)

    assert not has_cycle(e)
    clean(test_nodes)

    assert not has_cycle(f)
    clean(test_nodes)

    assert not has_cycle(g)
    clean(test_nodes)

    assert not is_tree(a)
    clean(test_nodes)

    assert not is_tree(b)
    clean(test_nodes)

    assert not is_tree(c)
    clean(test_nodes)

    assert not is_tree(d)
    clean(test_nodes)

    assert is_tree(e)
    clean(test_nodes)

    assert is_tree(f)
    clean(test_nodes)

    assert is_tree(g)
    clean(test_nodes)

    assert distance(a, g) == 4
    clean(test_nodes)

    assert distance(c, g) == 1
    clean(test_nodes)

    assert distance(e, d) is None
    clean(test_nodes)

    assert distance(e, d) is None
    clean(test_nodes)

    a = Vertex("A")
    a.succs = [a]

    assert has_cycle(a)

    a = Vertex("A")
    b = Vertex("B")
    c = Vertex("C")
    d = Vertex("D")
    e = Vertex("E")

    cyclic_test = [a, b, c, d, e]

    a.succs = [b, c]
    b.succs = [c]

    assert not has_cycle(a)
    clean(cyclic_test)

    a.succs = [b]
    b.succs = [c]
    c.succs = [a]

    assert has_cycle(a)
    clean(cyclic_test)

    a.succs = [b, c, d]
    b.succs = [e]
    c.succs = [d, e]
    d.succs = [c, e]

    assert has_cycle(a)
