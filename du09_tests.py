def set_size(node: Node, num: int) -> Node:
    node.size = num
    return node


def set_parents(node: Optional[Node], parent: Optional[Node]) -> None:
    if node is None:
        return None

    node.parent = parent

    set_parents(node.left, node)
    set_parents(node.right, node)

    return None


def test() -> None:
    tree = BSTree(set_size(Node(3), 3))
    tree.root.left = set_size(Node(2), 2)
    tree.root.left.left = Node(1)

    assert check_size(tree)
    assert not check_3_5_balanced(tree)

    tree = BSTree(set_size(Node(4), 5))
    tree.root.left = set_size(Node(2), 3)
    tree.root.right = set_size(Node(5), 1)

    tree.root.left.left = Node(1)
    tree.root.left.right = Node(3)

    assert check_size(tree)
    assert check_3_5_balanced(tree)

    tree = BSTree(set_size(Node(4), 9))
    tree.root.left = set_size(Node(3), 3)
    tree.root.right = set_size(Node(6), 5)

    tree.root.left.left = set_size(Node(2), 2)
    tree.root.left.left.left = Node(1)

    tree.root.right.left = Node(5)
    tree.root.right.right = set_size(Node(8), 3)

    tree.root.right.right.left = Node(7)
    tree.root.right.right.right = Node(9)

    assert check_size(tree)
    assert not check_3_5_balanced(tree)

    tree = BSTree(set_size(Node(4), 10))
    tree.root.left = set_size(Node(2), 4)
    tree.root.right = set_size(Node(6), 5)

    tree.root.left.left = set_size(Node(1), 2)
    tree.root.left.right = Node(3)

    tree.root.left.left.left = Node(0)

    tree.root.right.left = Node(5)
    tree.root.right.right = set_size(Node(8), 3)

    tree.root.right.right.left = Node(7)
    tree.root.right.right.right = Node(9)

    assert check_size(tree)
    assert check_3_5_balanced(tree)

    tree = BSTree(set_size(Node(3), 2))
    tree.root.left = set_size(Node(2), 1)

    assert check_size(tree)

    set_parents(tree.root, None)

    val = insert(tree, 1)
    assert val is not None and val.key == 3 and val.parent is None

    assert check_size(tree)

    tree = BSTree(set_size(Node(4), 7))
    tree.root.left = set_size(Node(3), 2)
    tree.root.right = set_size(Node(6), 4)

    tree.root.left.left = Node(2)

    tree.root.right.left = Node(5)
    tree.root.right.right = set_size(Node(8), 2)

    tree.root.right.right.left = Node(7)

    assert check_size(tree)

    set_parents(tree.root, None)

    val = insert(tree, 1)
    assert val is not None and val.key == 3 and val.parent.key == 4

    assert check_size(tree)

    tree = BSTree(set_size(Node(4), 7))
    tree.root.left = set_size(Node(2), 3)
    tree.root.right = set_size(Node(6), 3)

    tree.root.left.left = Node(1)
    tree.root.left.right = Node(3)

    tree.root.right.left = Node(5)
    tree.root.right.right = Node(7)

    assert check_size(tree)

    set_parents(tree.root, None)

    val = insert(tree, 0)
    assert val is None

    assert check_size(tree)

    val = insert(tree, 0)
    assert val is None

    assert check_size(tree)

    key_three = set_size(Node(4), 9)

    tree = BSTree(key_three)

    key_one = set_size(Node(3), 3)
    key_two = set_size(Node(6), 5)

    tree.root.left = key_one
    tree.root.right = key_two

    tree.root.left.left = set_size(Node(2), 2)
    tree.root.left.left.left = Node(1)

    tree.root.right.left = Node(5)
    tree.root.right.right = set_size(Node(8), 3)

    tree.root.right.right.left = Node(7)
    tree.root.right.right.right = Node(9)

    set_parents(tree.root, None)

    assert check_size(tree)

    rebalance(tree, key_one)

    assert check_size(tree)

    rebalance(tree, key_two)

    assert check_size(tree)

    rebalance(tree, key_three)
    draw_tree(tree, "test.txt")

    assert check_size(tree)

    tree = BSTree(set_size(Node(3), 2))
    tree.root.left = Node(2)

    insert(tree, 1)
