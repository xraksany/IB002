# remove lib before commit
from random import sample, randrange


def leaves_count(subtree: Optional[Node]) -> int:
    if subtree is None:
        return 0

    if subtree.left is None and subtree.right is None:
        return 1

    return leaves_count(subtree.left) + leaves_count(subtree.right)


def test() -> None:
    tree = BinTree()
    tree.root = Node(0)
    tree.root.left = Node(0)
    tree.root.right = Node(0)

    tree.root.left.left = Node(0)

    tree.root.right.left = Node(0)
    tree.root.right.right = Node(0)

    assert not check_left_align(tree)

    tree = BinTree()
    tree.root = Node(0)
    tree.root.left = Node(0)
    tree.root.left.right = Node(0)

    assert not check_left_align(tree)

    tree = BinTree()
    tree.root = Node(1)
    tree.root.left = Node(2)
    tree.root.right = Node(1)

    tree.root.left.left = Node(2)
    tree.root.left.right = Node(3)

    tree.root.right.right = Node(1)

    assert not check_left_align(tree)

    tree.root.right.right = None
    tree.root.right.left = Node(1)
    assert check_left_align(tree)

    tree = BinTree()
    tree.root = Node(1)
    tree.root.left = Node(2)
    tree.root.right = Node(1)

    tree.root.left.left = Node(2)
    tree.root.right.left = Node(1)

    assert not check_left_align(tree)

    tree = BinTree()
    tree.root = Node(0)

    tree.root.left = Node(0)
    tree.root.right = Node(0)

    tree.root.left.left = Node(0)
    tree.root.right.left = Node(0)

    tree.root.left.left.left = Node(0)
    tree.root.left.left.right = Node(0)

    tree.root.right.left.left = Node(0)

    assert not check_left_align(tree)

    tree = BinTree()
    tree.root = Node(1)
    tree.root.left = Node(2)
    tree.root.right = Node(1)

    tree.root.left.left = Node(2)
    tree.root.left.right = Node(3)

    tree.root.right.left = Node(1)

    assert check_min(tree)

    tree = BinTree()
    tree.root = Node(69)
    tree.root.left = Node(420)

    assert not check_min(tree)

    tree = build_min_tree([])
    assert tree.root is None

    tree = build_min_tree([42])
    assert tree.root is not None and tree.root.key == 42
    assert tree.root.left is None
    assert tree.root.right is None

    tree = build_min_tree([1, 2])
    assert tree.root is not None and tree.root.key == 1
    assert tree.root.left is not None and tree.root.left.key == 1
    assert tree.root.right is not None and tree.root.right.key == 2

    tree = build_min_tree([0, 0, 0, 0])
    assert tree.root is not None
    assert tree.root.key == 0

    assert tree.root.left is not None
    assert tree.root.left.key == 0

    assert tree.root.right is not None
    assert tree.root.right.key == 0

    assert tree.root.left.left is not None
    assert tree.root.left.left.key == 0

    assert tree.root.left.right is not None
    assert tree.root.left.right.key == 0

    assert tree.root.right.left is not None
    assert tree.root.right.left.key == 0

    assert tree.root.right.right is not None
    assert tree.root.right.right.key == 0

    tree = build_min_tree([2, 3, 1])
    assert tree.root is not None
    assert tree.root.key == 1

    assert tree.root.left is not None
    assert tree.root.left.key == 2

    assert tree.root.right is not None
    assert tree.root.right.key == 1

    assert tree.root.left.left is not None
    assert tree.root.left.left.key == 2

    assert tree.root.left.right is not None
    assert tree.root.left.right.key == 3

    assert tree.root.right.left is not None
    assert tree.root.right.left.key == 1

    tree = build_min_tree([2, 3, 1, 4, 3, 5])
    assert tree.root is not None
    assert tree.root.key == 1

    assert tree.root.left is not None
    assert tree.root.left.key == 1

    assert tree.root.right is not None
    assert tree.root.right.key == 3

    assert tree.root.left.left is not None
    assert tree.root.left.left.key == 2

    assert tree.root.left.right is not None
    assert tree.root.left.right.key == 1

    assert tree.root.right.left is not None
    assert tree.root.right.left.key == 3

    assert tree.root.left.left.left is not None
    assert tree.root.left.left.left.key == 2

    assert tree.root.left.left.right is not None
    assert tree.root.left.left.right.key == 3

    assert tree.root.left.right.left is not None
    assert tree.root.left.right.left.key == 1

    assert tree.root.left.right.right is not None
    assert tree.root.left.right.right.key == 4

    assert tree.root.right.left.left is not None
    assert tree.root.right.left.left.key == 3

    assert tree.root.right.left.right is not None
    assert tree.root.right.left.right.key == 5

    assert test1()
    # assert test2()
    # assert test3()
    assert test4()


def test1() -> bool:
    print("Positioning test: ", end="")

    leaves: List[int] = []
    for _ in range(1000):
        tree = build_min_tree(leaves)

        if leaves_count(tree.root) != len(leaves) or \
           not check_leaf_depth(tree):
            print("Incorrect\nError: ")
            print("Your leaves count: ", leaves_count(tree.root))
            print("Input leaves count: ", len(leaves))
            draw_tree(build_min_tree(leaves), "test.txt")
            return False

        leaves.append(0)

    print("Correct")
    return True


def test2() -> bool:
    print("Key test: ", end="")

    for _ in range(1000):
        leaves = sample(range(-10000, 10000), randrange(0, 10000))

        if not check_min(build_min_tree(leaves)):
            print("Incorrect\nError: ")
            draw_tree(build_min_tree(leaves), "test.txt")
            return False

    print("Correct")
    return True


def test3() -> bool:
    print("Left align tree test: ", end="")

    for _ in range(10000):
        leaves = sample(range(-1000, 1000), randrange(0, 1000))

        if not check_left_align(build_min_tree(leaves)):
            print("Incorrect\nError: ")
            draw_tree(build_min_tree(leaves), "test.txt")
            return False

    return True


def test4() -> bool:
    tree = BinTree()
    assert check_left_align(tree)

    tree.root = Node(0)
    assert check_left_align(tree)

    tree.root.left = Node(0)
    assert check_left_align(tree)

    tree.root.right = Node(0)
    assert check_left_align(tree)

    tree.root.right.left = Node(0)
    tree.root.left.left = Node(0)
    assert not check_left_align(tree)

    tree.root.left.right = Node(0)
    assert check_left_align(tree)

    tree.root.left.left.left = Node(0)
    tree.root.left.left.right = Node(0)

    tree.root.left.right.left = Node(0)

    tree.root.right.left.left = Node(0)
    assert not check_left_align(tree)

    tree.root.left.right.right = Node(0)
    assert check_left_align(tree)

    tree.root.right.right = Node(0)
    tree.root.right.right.left = Node(0)
    assert not check_left_align(tree)

    tree.root.right.left.right = Node(0)
    assert check_left_align(tree)

    tree.root.left.left.right = None
    assert not check_left_align(tree)

    tree.root.left.left.right = Node(0)
    tree.root.left.right.right = None
    assert not check_left_align(tree)

    tree.root.left.right = None
    tree.root.right.right = None
    tree.root.right.left.right = None
    assert not check_left_align(tree)

    print("Correct")

    return True
